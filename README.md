# Pre-Evaluation: A Simple User Registration Module
**Content:**
 1. Application Tier: Java Spring Boot.
 2. Presentation Tier: Angular, Javascript, Bootstrap, HTML, CSS.
 3. Data-tier: Oracle/MySQL.
